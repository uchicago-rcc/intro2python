# Introduction to Python Programming

Material for the Introduction to Python Programming Workshop

## Outline of Topics Covered:
  * Jupyter Lab IDE & Notebook Basics
  * Data Structures in Python
  * Loops and Control Flow
  * Functions
  * File I/O
  * Plotting and Visualization

## Running the Collection of Tutorials

There are no python installation requirements for your local computer in order to 
view and run any of the ipython notebooks used in this short course. Instead we
will rely on running the collection of notebooks from midway. If you do not have 
a RCC account, a guest account will be provided for the duration of the workshop. 

### Connecting to Midway2

In order to connect to midway2 you will need a ssh client. Please see the
[Connecting to Midway2](docs/connecting_midway.pdf) document to find the 
instructions for setting up a client and connecing to midway. The hostname for 
midway2 that we will connect to is the following: 

```bash
midway2.rcc.uchicago.edu 
```

### Launching JupyterLab 

Once you are connected to midway2, you will need to clone this git repository 
to your home folder. To do so, issue the following from the terminal while logged
into midway: 

```bash
git clone https://gitlab.com/jhskone/intro2python.git
```
Then change directory to theh repository and launch the jupyterlab session:

```bash
cd intro2python
./launch-nb.sh
```

Follow the instructions print to the terminal. You will need to copy and paste
the URL along with the token into your web browser in order to launch the lab session.

### Alternative way to interact with material
Alternatively one could use cloud compute resources through
[binder](), however the resources are not garaunteed and will depend on the availabilty 
at any particular time. In order to access an interactive environment to view and 
execute the python programming jupyter notebooks, you would need to click the
following binder link: 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jhskone%2Fintro2python/master?urlpath=lab/tree/master.ipynb)
Use this as an alternative if you are unable to setup a client to connect to midway.

## Using Python on Your Personal Computer

Most of your personal computers will come with python 2.7 already installed, but 
to take full advantage of newer features and keep yourself contemporary, you'll 
need to upgrade yourself to at least version 3.5 of python. The end-of-life for 
the 3.5 version of python (i.e. the time at which no further updates or patches 
will be released is 09,13,2020). For reference the newest stable release 3.x version
of python (3.7) has an end-of-life scheduled for 06,27,2023.

### Installing Anaconda Python
**Note:** *You do not need to install anything to view and execute the examples 
for this workshop. The installation instructions provided here are for 
you to be able to run python on your own personal resource if you wish to in the 
future.*

There are several ways in which one can go about installing or upgrading python
on their system. The [python software foundation](https://www.python.org/) is responsible for managing and 
distributing various releases of python. You could directly download from their 
site a version of python for your particular OS, but this is is not the recommended
way of upgrading yourself. You could also use a package manager if using linux 
(yum, apt-get, etc.) or a mac (home brew, mac ports, etc.) to system install 
python, but again this is not the easiest solution, requires root access on the
machine, and furthermore can complicate your managment of python on your system. 

The simplest and recommended solution is for you to install the Anaconda 
distribution of python. <br/>
 ![ |small ](imgs/anaconda.png)

Why Anaconda?...<br/>
The Anaconda distribution of python comes prepackaged with most of the python 
packages you would want to use for scientific computing, like numpy, matplotlib, 
scipy, and jupyter notebook server/lab. Furthermore it comes packaged with 
Intel's MKL math libraries that the numpy and scipy functions link against, 
providing you with faster linear algebra under the hood without any additional 
effort on your part to configure this. 

Follow the [Anaconda Python Installation Instructions](https://docs.anaconda.com/anaconda/install/)
provided on the Continuum Analytics site for your particular operating system. 
Download a 3.x version (either the latest or a version no older than 3.5). The 
miniconda version of Anaconda provides a bare bones install of python with the 
conda package manager. It then requires you to install all of the packages you 
require. It is therefore not recommended that you install the miniconda version
as this does not come out of the box with any of the scientific packages preinstalled. 

